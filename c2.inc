#!/bin/sh
# Copyright (c) 2016--2021 TK Chia
#
# This file is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation; either version 2.1 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.

conf_mod c.common c
conf_Is_multiple_host=yes

conf__c2_chk_compiler_with() {
	local cflags ldlibs
	cflags="$1"
	ldlibs="$2"
	shift 2
	if conf__cc_chk_compiler_with "$CC2" "$CPPFLAGS2" "$cflags" \
	    "$LDFLAGS2" "$ldlibs" "$conf_Host2_exec" "$conf_Host_exe_ext" \
	    ${1+"$@"}
	then	return 0
	else	return $?
	fi
}

conf__c2_compile_prog() {
	local prog res
	conf_c2_chk_compiler_and_executor
	if conf__in_help_mode
		then return 0; fi
	prog="$1"
	shift
	src="$prog.c"
	(
		for line in ${1+"$@"}
			do echo "$line"; done
	) >"$src"
	res=0
	$CC2 $CPPFLAGS2 $CFLAGS2 $LDFLAGS2 -o"$prog" "$src" $LDLIBS2 \
	    >/dev/null 2>&1 || res=1
	rm -f "$src"
	return $res
}

conf_c2_chk_compiler_and_executor() {
	local save_ifs arch rest ld_prefix
	if test -n "$conf_Have_c2_compiler"
		then return 0; fi
	conf__add_var CC2 CPPFLAGS2 CFLAGS2 LDFLAGS2 LDLIBS2 \
	    conf_Have2_c_compiler conf_Host2_exec conf_Host2_exe_ext
	if conf__in_help_mode
		then return 0; fi
	conf__probe_exe_ext host-2 "$conf_Cross_tag" conf_Host2_exe_ext
	conf__blare_test 'deciding C compiler for host-2'
	if test -n "$conf_Cross2_tag" -a -z "$CC2"; then
		conf__cc_find_cross_compiler "$conf_Cross2_tag" CC2 CFLAGS2
	elif test -z "$CC2"; then
		: "${CC2=$CC}"
		: "${CPPFLAGS2=$CPPFLAGS}"
		: "${CFLAGS2=$CFLAGS}"
		: "${LDFLAGS2=$LDFLAGS}"
		: "${LDLIBS2=$LDLIBS}"
	else
		: "${CFLAGS2=-O3}"
	fi
	conf__blare_res "$CC2 $CPPFLAGS2 $CFLAGS2"
	conf__blare_test 'deciding host-2 program executor'
	if test set != "${conf_Host2_exec+set}"; then
		conf__cc_find_execor host-2 "$conf_Cross2_tag" \
		    "$CC2" "$CPPFLAGS2" "$CFLAGS2" "$LDFLAGS2" "$LDLIBS2" \
		    "$conf_Host2_exe_ext" conf_Host2_exec
	fi
	conf__blare_res "${conf_Host2_exec:-(blank)}"
	conf__blare_test 'checking if C compiler and executor for host-2 work'
	if conf__c2_chk_compiler_with "$CFLAGS2" "$LDLIBS2"; then
		conf__blare_res yes
		conf_Have_c2_compiler=yes
	else
		conf__blare_res no
		conf__barf 'C compiler and/or executor does not work!'
	fi
	conf__cc_probe_thumb host-2 "$CC2" "$CPPFLAGS2" CFLAGS2 "$LDFLAGS2" \
	    "$LDLIBS2" "$conf_Host2_exec" "$conf_Host2_exe_ext"
	conf__cc_probe_lbss_fix host-2 "$CC2" "$CPPFLAGS2" "$CFLAGS2" \
	    LDFLAGS2 "$LDLIBS2" "$conf_Host2_exec" "$conf_Host2_exe_ext"
}

conf_c2_probe_opt_fno_integrated_as() {
	conf_c2_chk_compiler_and_executor
	if conf__in_help_mode
		then return 0; fi
	conf__cc_probe_opt -fno-integrated-as host-2 "$CC2" "$CPPFLAGS2" \
	    "$CFLAGS2" "$LDFLAGS2" "$LDLIBS2" "$conf_Host2_exec" \
	    "$conf_Host2_exe_ext" conf_Have_c2_opt_fno_integrated_as
}

conf_c2_probe_opt_fcf_protection_none() {
	conf_c2_chk_compiler_and_executor
	if conf__in_help_mode
		then return 0; fi
	conf__cc_probe_opt -fcf-protection=none host-2 "$CC2" "$CPPFLAGS2" \
	    "$CFLAGS2" "$LDFLAGS2" "$LDLIBS2" "$conf_Host2_exec" \
	    "$conf_Host2_exe_ext" conf_Have_c2_opt_fcf_protection_none
}

conf_c2_probe_macro() {
	local macro var
	conf_c2_chk_compiler_and_executor
	if conf__in_help_mode
		then return 0; fi
	macro="$1"
	shift
	var="conf_Have_c2_macro_`conf__escape "$macro"`"
	conf__add_var "$var"
	conf__blare_test "testing for host-2-side C macro $macro"
	if conf__c2_chk_compiler_with "$CFLAGS2" "$LDLIBS2" ${1+"$@"} \
	    "#ifndef $macro" "#error" "#endif"
	then    eval $var=yes
	else    eval $var=no
	fi
	eval conf__blare_res \"\$$var\"
}

conf_c2_chk_macro() {
	local var macro
	macro="$1"
	conf_c2_probe_macro ${1+"$@"}
	if conf__in_help_mode
		then return 0; fi
	var="conf_Have_c2_macro_`conf__escape "$macro"`"
	if eval test yes = \"\$$var\"; then :; else
		conf__barf "macro $1 not found host-2-side!"; fi
}
